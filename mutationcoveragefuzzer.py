from mutationfuzzer import *
from processcoveragerunner import *


curr_dir = os.getcwd()


def population_coverage(cmd, population):
    cumulative_coverage = []
    all_coverage = set()

    for s in population:
        # Execute and send commands
        SCH_TRX_PORT_TM = 9  # ///< Telemetry port
        SCH_TRX_PORT_TC = 10  # ///< Telecommands port
        SCH_TRX_PORT_RPT = 11  # ///< Digirepeater port (resend packets)
        SCH_TRX_PORT_CMD = 12  # ///< Commands port (execute console commands)
        SCH_TRX_PORT_DBG = 13

        fs_dir = "../Git/SUCHAI-Seminario/"

        # Compile fs
        os.chdir(fs_dir)
        compilation_cmd = "python3 compile.py LINUX X86 --fp 0 --hk 0 --st_mode 2 --buffers_csp 500"
        os.system(compilation_cmd)

        # Run zmqhub.py
        ex_zmqhub = Popen(["python3", "sandbox/csp_zmq/zmqhub.py", "--ip", "127.0.0.1", "--proto", "tcp"], stdin=PIPE)

        os.chdir("build_x86")

        dest = "1"
        addr = "9"
        port = str(SCH_TRX_PORT_CMD)
        node = FuzzCspZmqNode(addr, hub_ip="127.0.0.1", proto="tcp")
        node.start()

        # Execute flight software
        time.sleep(1)
        suchai_process = Popen(["./SUCHAI_Flight_Software"], stdin=PIPE)
        time.sleep(4)

        # node.wait_init_ready()

        # Clean database
        print("node send: drp_ebf 1010")  # For debugging purposes
        header = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=55)
        node.send_message("drp_ebf 1010", header)

        print("node send:", cmd + " " + s)  # For debugging purposes
        header = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=55)
        # node.send_message(cmd + " " + params, header)
        node.send_message(cmd + " " + s, header)

        # Exit SUCHAI process
        hdr = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=56)
        node.send_message("obc_reset", hdr)

        # Get SUCHAI process return code
        return_code = suchai_process.wait()
        print("Return code: ", return_code)  # For debugging purposes

        # Get commands, results, execution time and memory usage
        node.stop()

        # Kill zmqhub.py
        ex_zmqhub.kill()

        # Define directory where gcno y gcda files are
        gcov_dir = "/home/tamara/Git/SUCHAI-Seminario/build_x86/CMakeFiles/SUCHAI_Flight_Software.dir/home/tamara/Git/SUCHAI-Seminario/"
        os.chdir(curr_dir + "/Files/")
        for path in Path(gcov_dir).rglob('*.c.o'):
            print(path)
            os.system("gcov " + str(path))

        os.chdir(curr_dir)
        all_coverage |= read_gcov_coverage("Files/")
        cumulative_coverage.append(len(all_coverage))

    return all_coverage, cumulative_coverage


class MutationCoverageFuzzer(MutationFuzzer):
    def reset(self):
        super().reset()
        self.coverages_seen = set()
        # Now empty; we fill this with seed in the first fuzz runs
        self.population = []

    def run(self, runner):
        """Run process(inp) while tracking coverage.
           If we reach new coverage,
           add inp to population and its coverage to population_coverage
        """
        result, outcome = super().run(runner)
        print("outcomes")
        print((result, outcome))
        new_coverage = frozenset(runner.coverage())
        if outcome == Runner.PASS and new_coverage not in self.coverages_seen:
            print("new_cov")
            # We have new coverage
            self.population.append(self.inp)
            self.coverages_seen.add(new_coverage)

        return result


if __name__ == "__main__":
    zmqhub_dir = "../Git/SUCHAI-Seminario/sandbox/csp_zmq/zmqhub.py"
    seed_input = "com_send_cmd 0 1"
    mutation_fuzzer = MutationCoverageFuzzer(seed=[seed_input])
    # Run zmqhub.py
    ex_zmqhub = Popen(["python3", zmqhub_dir, "--ip", "127.0.0.1", "--proto", "tcp"], stdin=PIPE)

    prev_dir = os.getcwd()
    exec_dir = "/home/tamara/Git/SUCHAI-Memoria/build_x86/"
    exec_cmd = "./SUCHAI_Flight_Software"

    os.chdir(exec_dir)
    SUCHAI_process = ProcessCoverageRunner(exec_cmd)
    mutation_fuzzer.runs(SUCHAI_process, trials=50)

    os.chdir(prev_dir)

    # Kill zmqhub.py
    ex_zmqhub.kill()

    print("cmd_name: ", mutation_fuzzer.cmd_name)
    print("population: ", mutation_fuzzer.population)

    all_coverage, cumulative_coverage = population_coverage(mutation_fuzzer.cmd_name, mutation_fuzzer.population)

    print(all_coverage)
    print(cumulative_coverage)

    maxi = max(cumulative_coverage)
    mini = min(cumulative_coverage)

    plt.figure()
    plt.plot(cumulative_coverage, 'bo')
    plt.title('Coverage of new lines of SUCHAI flight software with command com_send_data and random parameters')
    plt.xlabel('# of inputs')
    plt.ylabel('lines covered')
    plt.xlim(0, len(cumulative_coverage) + 1)
    plt.ylim(0, maxi + 10)
    plt.savefig("coverage_evolution_mutation_50_com_send_cmd_2.png", bbox_inches='tight')

    plt.figure()
    plt.plot(cumulative_coverage, 'bo')
    plt.title('Coverage of new lines of SUCHAI flight software with command com_send_data and random parameters')
    plt.xlabel('# of inputs')
    plt.ylabel('lines covered')
    plt.xlim(0, len(cumulative_coverage) + 1)
    plt.ylim(mini-10, maxi + 10)
    plt.savefig("coverage_evolution_mutation_50_com_send_cmd_zoom_2.png", bbox_inches='tight')