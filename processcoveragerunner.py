from processrunner import *


def read_gcov_coverage(dir):
    coverage = set()
    for gcov_file in os.listdir(dir):
        with open(dir + gcov_file) as file:
            for line in file.readlines():
                elems = line.split(':')
                covered = elems[0].strip()
                line_number = int(elems[1].strip())
                if covered.startswith('-') or covered.startswith('#'):
                    continue
                coverage.add((gcov_file, line_number))
    return coverage


class ProcessCoverageRunner(ProcessRunner):
    def run_process(self, cmd_params):
        result = super().run_process(cmd_params)
        print("result: ", result)

        # Define directory where gcno y gcda files are
        gcov_dir = "/home/tamara/Git/SUCHAI-Seminario/build_x86/CMakeFiles/SUCHAI_Flight_Software.dir/home/tamara/Git/SUCHAI-Seminario/"
        curr_dir = "/home/tamara/SUCHAI-Coverage"

        for path in Path(gcov_dir).rglob('*.c.o'):
            print(path)
            os.system("gcov " + str(path))

        self._coverage = read_gcov_coverage(curr_dir + "/Files/")
        print(self._coverage)
        return result

    def coverage(self):
        return self._coverage


if __name__ == "__main__":
    zmqhub_dir = "../Git/SUCHAI-Seminario/sandbox/csp_zmq/zmqhub.py"
    cmd = "com_ping 1"
    # Run zmqhub.py
    ex_zmqhub = Popen(["python3", zmqhub_dir, "--ip", "127.0.0.1", "--proto", "tcp"], stdin=PIPE)

    prev_dir = os.getcwd()
    exec_dir = "/home/tamara/Git/SUCHAI-Memoria/build_x86/"
    exec_cmd = "./SUCHAI_Flight_Software"

    os.chdir(exec_dir)
    SUCHAI_process = ProcessCoverageRunner(exec_cmd)
    SUCHAI_process.run(cmd)

    os.chdir(prev_dir)

    # Kill zmqhub.py
    ex_zmqhub.kill()

    print(list(SUCHAI_process.coverage()))