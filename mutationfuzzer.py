from fuzzingbook.Fuzzer import Fuzzer
import random
import os

def delete_random_character(params):
    """Returns params with a random character deleted"""
    if params == "":
        return params

    pos = random.randint(0, len(params) - 1)
    # print("Deleting", repr(s[pos]), "at", pos)
    return params[:pos] + params[pos + 1:]


def change_random_character(params):
    """Returns params with a random character changed"""
    if params == "":
        return ""

    pos = random.randint(0, len(params) - 1)
    random_character = chr(random.randrange(32, 64))
    return params[:pos] + random_character + params[pos + 1:]


def insert_random_character(params):
    """Returns params with a random character inserted"""
    pos = random.randint(0, len(params))
    random_character = chr(random.randrange(32, 64))
    return params[:pos] + random_character + params[pos:]


def mutate(s):
    """Return s with a random mutation applied"""
    mutators = [
        delete_random_character,
        insert_random_character,
        change_random_character
    ]
    mutator = random.choice(mutators)
    # print(mutator)
    return mutator(s)

class MutationFuzzer(Fuzzer):
    def __init__(self, seed, min_mutations=600, max_mutations=1000):
        self.seed = [" ".join((cmd_params.split(" ")[1:])) for cmd_params in seed]
        self.min_mutations = min_mutations
        self.max_mutations = max_mutations
        self.cmd_name = seed[0].split(" ")[0]
        self.reset()

    def reset(self):
        self.population = self.seed
        self.seed_index = 0

    def mutate(self, inp):
        return mutate(inp)

    def create_candidate(self):
        random.seed()
        candidate = random.choice(self.population)
        trials = random.randint(self.min_mutations, self.max_mutations)
        for i in range(trials):
            candidate = self.mutate(candidate)
        print("location: ", os.getcwd())
        with open("/home/tamara/SUCHAI-Coverage/cov_evol_mut_com_send_cmd_50inputs_2.txt", "a") as results:
            results.write(self.cmd_name + " " + candidate + "\n")
        return candidate

    def fuzz(self):
        if self.seed_index < len(self.seed):
            # Still seeding
            self.inp = self.seed[self.seed_index]
            self.seed_index += 1
        else:
            # Mutating
            self.inp = self.create_candidate()
        return self.cmd_name + " " + self.inp


seed_input = "tm_send_status"
mutation_fuzzer = MutationFuzzer(seed=[seed_input])
print(mutation_fuzzer.fuzz())
print(mutation_fuzzer.fuzz())
print(mutation_fuzzer.fuzz())
