from subprocess import Popen, PIPE
from fuzzcspzmqnode import *
from fuzzingbook.Fuzzer import Runner
import time
import os
from pathlib import Path
from randomcommandfuzzer import *
import matplotlib.pyplot as plt
import sys


SCH_TRX_PORT_TM = 9               # ///< Telemetry port
SCH_TRX_PORT_TC = 10               # ///< Telecommands port
SCH_TRX_PORT_RPT = 11               # ///< Digirepeater port (resend packets)
SCH_TRX_PORT_CMD = 12               # ///< Commands port (execute console commands)
SCH_TRX_PORT_DBG = 13               # ///< Debug port, logs output


class ProcessRunner(Runner):
    def __init__(self, exec_cmd="./SUCHAI_Flight_Software"):
        self.exec_cmd = exec_cmd

    def run_process(self, cmd_params):
        fs_dir = "../../SUCHAI-Seminario/"

        # Compile fs
        os.chdir(fs_dir)
        compilation_cmd = "python3 compile.py LINUX X86 --fp 0 --hk 0 --st_mode 2 --buffers_csp 500"
        os.system(compilation_cmd)

        os.chdir("build_x86")

        dest = "1"
        addr = "9"
        port = str(SCH_TRX_PORT_CMD)
        node = FuzzCspZmqNode(addr, hub_ip="127.0.0.1", proto="tcp")
        node.start()

        # Execute flight software
        time.sleep(1)
        suchai_process = Popen(["./SUCHAI_Flight_Software"], stdin=PIPE)
        time.sleep(4)

        # Clean database
        print("node send: drp_ebf 1010")  # For debugging purposes
        header = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=55)
        node.send_message("drp_ebf 1010", header)

        print("node send:", cmd_params)  # For debugging purposes
        header = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=55)
        # node.send_message(cmd + " " + params, header)
        node.send_message(cmd_params, header)

        # Exit SUCHAI process
        hdr = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=56)
        node.send_message("obc_reset", hdr)

        # Get SUCHAI process return code
        return_code = suchai_process.wait()
        print("Return code: ", return_code)  # For debugging purposes

        # Get commands, results, execution time and memory usage
        node.stop()

        return return_code

    def run(self, inp):
        try:
            result = self.run_process(inp)
            outcome = self.PASS
        except Exception:
            result = None
            outcome = self.FAIL

        return result, outcome
