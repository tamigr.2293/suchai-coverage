from fuzzingbook.Fuzzer import RandomFuzzer
import random
import os


class RandomParamsFuzzer(RandomFuzzer):
    def __init__(self, cmd_name, fs_cmds_nparams_dic, min_length=10, max_length=100, char_start=32, char_range=32):
        RandomFuzzer.__init__(self, min_length, max_length, char_start, char_range)
        self.fs_cmds_nparams_dic = fs_cmds_nparams_dic
        self.cmd_name = cmd_name

    def fuzz_command_params(self):
        """
        Produce random command with random parameters. The command is randomly chosen from a list.
        :return: String. Commands with paramaeters to send
        """
        random.seed()
        cmd = self.cmd_name
        n_params = self.fs_cmds_nparams_dic[cmd]
        params_list = [self.fuzz() for i in range(n_params)]
        params = " ".join(params_list)
        print("location: ", os.getcwd())
        with open("cov_evol_50inputs_3.txt", "a") as results:
            results.write(cmd + " " + params + "\n")
        return cmd + " " + params