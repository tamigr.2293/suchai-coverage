import time
from subprocess import PIPE, Popen
from subprocess import Popen, PIPE
from fuzzcspzmqnode import *
import time
import glob,os
from pathlib import Path
from randomcommandfuzzer import *
import matplotlib.pyplot as plt
import sys

curr_dir = os.getcwd()

def read_gcov_coverage(dir):
    coverage = set()
    for gcov_file in os.listdir(dir):
        with open(dir+gcov_file) as file:
            for line in file.readlines():
                elems = line.split(':')
                covered = elems[0].strip()
                line_number = int(elems[1].strip())
                if covered.startswith('-') or covered.startswith('#'):
                    continue
                coverage.add((gcov_file, line_number))
    return coverage

def main():
    # Get lists of all the flight software commands, and number of parameters that each one receives
    fs_cmds_nparams_dic = {}
    seen_modules = []

    with open("suchai_cmd_list_all.csv") as f:  # in this file 3 commands were excluded
        for row in f:
            cmd = row.split(', ')[0]
            fs_cmds_nparams_dic[cmd] = (int(row.split(', ')[1]), cmd.split('_')[0])

    cmds_list = list(fs_cmds_nparams_dic.keys())
    random.seed()
    random.shuffle(cmds_list)

    chosen_cmds_dict = {}
    for elem in cmds_list:
        if elem == "com_send_cmd":
            chosen_cmds_dict[elem] = fs_cmds_nparams_dic[elem][0]
            seen_modules.append(fs_cmds_nparams_dic[elem][1])
            break
        if not fs_cmds_nparams_dic[elem][1] in seen_modules:
            chosen_cmds_dict[elem] = fs_cmds_nparams_dic[elem][0]
            seen_modules.append(fs_cmds_nparams_dic[elem][1])

    runs = 1
    trials = 50
    maxi = 0  # Maximum coverage
    mini = sys.maxsize # Minimum coverage
    average_coverages = {}  # Dictionary of average coverage lists with their command name

    print("chosen: ")
    print(chosen_cmds_dict)
    print(len(list(chosen_cmds_dict.keys())))

    for cmd_name in list(chosen_cmds_dict.keys()):
        sum_coverage = [0] * trials

        # Initialize Fuzzer
        random_fuzzer = RandomParamsFuzzer(cmd_name, chosen_cmds_dict, min_length=0, max_length=10, char_start=33,
                                       char_range=93)
        for run in range(runs):

            population = []
            for i in range(trials):
                population.append(random_fuzzer.fuzz_command_params())

            cumulative_coverage = []
            all_coverage = set()

            for s in population:
                print("COMMAND_ITER: ", list(chosen_cmds_dict.keys()).index(cmd_name))
                # Execute and send commands
                SCH_TRX_PORT_TM = 9               # ///< Telemetry port
                SCH_TRX_PORT_TC = 10               # ///< Telecommands port
                SCH_TRX_PORT_RPT = 11               # ///< Digirepeater port (resend packets)
                SCH_TRX_PORT_CMD = 12               # ///< Commands port (execute console commands)
                SCH_TRX_PORT_DBG = 13

                fs_dir = "../Git/SUCHAI-Seminario/"

                # Compile fs
                os.chdir(fs_dir)
                compilation_cmd = "python3 compile.py LINUX X86 --fp 0 --hk 0 --st_mode 2 --buffers_csp 500"
                os.system(compilation_cmd)

                # Run zmqhub.py
                ex_zmqhub = Popen(["python3", "sandbox/csp_zmq/zmqhub.py", "--ip", "127.0.0.1", "--proto", "tcp"], stdin=PIPE)

                os.chdir("build_x86")

                dest = "1"
                addr = "9"
                port = str(SCH_TRX_PORT_CMD)
                node = FuzzCspZmqNode(addr, hub_ip="127.0.0.1", proto="tcp")
                node.start()

                # Execute flight software
                time.sleep(1)
                init_time = time.time()  # Start measuring execution time of the sequence
                suchai_process = Popen(["./SUCHAI_Flight_Software"], stdin=PIPE)
                time.sleep(4)

                # node.wait_init_ready()

                # Clean database
                print("node send: drp_ebf 1010")  # For debugging purposes
                header = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=55)
                node.send_message("drp_ebf 1010", header)

                # cmds_list = ["com_ping", "com_ping"]
                # params_list = ["1", "1", "1"]
                # Start sendind random commands
                #cmd_params = random_fuzzer.fuzz_command_params() # Choose random command + params
                #for i in range(0, len(cmds_list)):
                # time.sleep(0.5)  # Give some time to zmqnode threads (writer and reader)
                # cmd = cmds_list[i]
                # params = params_list[i]
                #print("node send:", cmd + " " + params)  # For debugging purposes
                print("node send:", s)  # For debugging purposes
                header = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=55)
                # node.send_message(cmd + " " + params, header)
                node.send_message(s, header)

                # Exit SUCHAI process
                hdr = CspHeader(src_node=int(addr), dst_node=int(dest), dst_port=int(port), src_port=56)
                node.send_message("obc_reset", hdr)

                # Get SUCHAI process return code
                return_code = suchai_process.wait()
                end_time = time.time()  # End measuring execution time of the sequence
                print("Return code: ", return_code)  # For debugging purposes

                # Get commands, results, execution time and memory usage
                node.stop()

                # Kill zmqhub.py
                ex_zmqhub.kill()

                # Define directory where gcno y gcda files are
                gcov_dir = "/home/tamara/Git/SUCHAI-Seminario/build_x86/CMakeFiles/SUCHAI_Flight_Software.dir/home/tamara/Git/SUCHAI-Seminario/"
                os.chdir(curr_dir+"/Files/")
                for path in Path(gcov_dir).rglob('*.c.o'):
                    print(path)
                    os.system("gcov "+str(path))

                os.chdir(curr_dir)
                all_coverage |= read_gcov_coverage("Files/")
                cumulative_coverage.append(len(all_coverage))

            assert len(cumulative_coverage) == trials
            for i in range(trials):
                sum_coverage[i] += cumulative_coverage[i]

        average_coverage = []
        for i in range(trials):
            average_coverage.append(sum_coverage[i] / runs)

        maxi = max(maxi, max(average_coverage))
        mini = min(mini, min(average_coverage))
        average_coverages[cmd_name] = average_coverage

    plt.figure(figsize=(18, 8))
    for command_name in average_coverages.keys():
        plt.plot(average_coverages[command_name], label=command_name, alpha=0.4)
    plt.title("Average coverage of commands with random inputs")
    plt.xlabel('# of inputs')
    plt.ylabel('lines covered')
    plt.xlim(0, trials - 1)
    plt.ylim(0, maxi + 100)
    plt.legend(loc=3,bbox_to_anchor=(1,0), fontsize=9)
    plt.subplots_adjust(left=0.05)
    plt.savefig("coverage_evolution_50inputs_3.png", bbox_inches='tight')

    plt.figure(figsize=(18, 8))
    for command_name in average_coverages.keys():
        plt.plot(average_coverages[command_name], label=command_name, alpha=0.4)
    plt.title("Average coverage of commands with random inputs")
    plt.xlabel('# of inputs')
    plt.ylabel('lines covered')
    plt.xlim(0, trials - 1)
    plt.ylim(mini-10, maxi + 10)
    plt.legend(loc=3, bbox_to_anchor=(1, 0), fontsize=9)
    plt.subplots_adjust(left=0.05)
    plt.savefig("coverage_evolution_50inputs_3_zoom.png", bbox_inches='tight')

if __name__ == "__main__":
    main()