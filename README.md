# SUCHAI-Coverage
El objetivo de este proyecto es rastrear la ejecución del software de vuelo de SUCHAI usando técnicas de fuzzing. En particular se definen las siguientes implementaciones:
* Medición y análisis de coverage generando casos totalmente aleatorios.
* Medición y análisis de coverage y guía de generación de casos de prueba a través de mutación .

Para correr los resultados de la primera implementación ejecute el archivo main_cov.py con python3. El número de inputs y de experimentos se guardan en las variables trials y runs de ese mismo archivo respectivamente. Al final del código verá generaciones de dos gráficos dentro de ese mismo directorio. Verifique los paths fs_dir y gcov_dir coinciden con la dirección donde ubica sus carpeta y archivos. 

Para correr los resultados de la segunda implementación ejecute el archivo mutationcoveragefuzzer.py con python3. El número de inputs se guarda en las variables trials. Al final del código verá generaciones de dos gráficos dentro de ese mismo directorio. Verifique los paths fs_dir y gcov_dir coinciden con la dirección donde ubica sus carpeta y archivos. 

